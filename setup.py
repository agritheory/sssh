# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in sssh/__init__.py
from sssh import __version__ as version

setup(
	name='sssh',
	version=version,
	description='ERPNext Customizations for Somali Sudanese Specialized Hospital',
	author='Somali Sudanese Specialized Hospital',
	author_email='tyler@agritheory.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
