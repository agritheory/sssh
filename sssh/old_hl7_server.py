import hl7apy
import socket
from hl7apy.mllp import MLLPServer, AbstractHandler
# from colorama import Fore, Back, Style

HOST = socket.gethostname()
PORT = 2575

class PDQHandler(AbstractHandler):
	def reply(self):
		return PDQ_RES


class ErrorHandler(AbstractHandler):
	def __init__(self, exc, msg):
		super(ErrorHandler, self).__init__(msg)
		self.exc = exc

	def reply(self):
		if isinstance(self.exc, InvalidHL7Message):
			return INVALID_MESSAGE
		elif isinstance(self.exc, UnsupportedMessageType):
			return UNSUPPORTED_MESSAGE


class CustomArgsPDQHandler(AbstractHandler):
	def __init__(self, msg, is_pdqv):
		super(CustomArgsPDQHandler, self).__init__(msg)
		self.is_pdqv = is_pdqv

	def reply(self):
		if self.is_pdqv:
			return PDQV_RES
		return PDQ_RES

HANDLERS = {
	'QBP^Q22^QBP_Q21': (PDQHandler,) # value is a tuple
}


def start_hl7_server(host=HOST, port=PORT, handlers=HANDLERS):
	server = MLLPServer(host, port, handlers, timeout=3)
	server.allow_reuse_address = True
	print(Fore.GREEN + 'Starting MLLP Server')
	server.serve_forever()

def start_hl7_server():
	import time
    import systemd.daemon

    print('Starting up ...')
    time.sleep(10)
    print('Startup complete')
    systemd.daemon.notify('READY=1')

    while True:
        print('Hello from the Python Demo Service')
        time.sleep(5)


#
# class ThreadedHL7Server(ThreadingMixIn, TCPServer):
# 	pass
#
#
#
# def hl7_server(bootinfo):
# 	HOST, PORT = socket.gethostname(), 2575
# 	server = ThreadedHL7Server((HOST, PORT), HL7Handler)
# 	with server:
# 		ip, port = server.server_address
# 		server_thread = threading.Thread(target=server.serve_forever)
# 		# Exit the server thread when the main thread terminates
# 		server_thread.daemon = True
# 		server_thread.start()
# 		print("Server loop running in thread:", server_thread.name)
# 		server.serve_forever()
# 	return bootinfo
#
#
# class HL7Handler(BaseRequestHandler):
# 	def handle(self):
# 		data = str(self.request.recv(1024), 'ascii')
# 		cur_thread = threading.current_thread()
# 		response = bytes("{}: {}".format(cur_thread.name, data), 'ascii')
# 		self.request.sendall(response)
#
#
# def hl7_server(bootinfo):
# 	print(Fore.GREEN + 'Starting MLLP Server')
# 	server = TCPServer(('localhost', 2575), HL7Handler, bind_and_activate=True)
# 	server.allow_reuse_address = True
# 	server.serve_forever()
# 	return bootinfo




#
#
# class PDQHandler(AbstractHandler):
# 	def reply(self):
# 		msg = parse_message(self.incoming_message)
# 		# do something with the message
# 		res = Message('RSP_K21')
# 		# populate the message
# 		return res.to_mllp()
#
# handlers = {
# 	'QBP^Q22^QBP_Q21': (PDQHandler,) # value is a tuple
# 	}
#
#
# def find_open_ports():
#     for port in range(1, 8081):
#         with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
#             res = sock.connect_ex(('localhost', port))
#             if res == 0:
#                 yield port
#
#
# def hl7_server(bootinfo):
# 	# check if port is in use
# 	with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
# 		print(Fore.GREEN + str(s.connect_ex(('localhost', 2575)) == 0))
# 		if s.connect_ex(('localhost', 2575)) != 0:
# 			print(Fore.GREEN + str(s.connect_ex(('localhost', 2575)) == 0))
# 			print(Fore.GREEN + 'MLLP Server already running on 2575')
# 			return bootinfo
# 	print(Fore.GREEN + 'Starting MLLP Server')
# 	server = MLLPServer('localhost', 2575, handlers)
# 	server.daemon_threads = True
# 	server.serve_forever()
# 	# http://crs4.github.io/hl7apy/tutorial/index.html#mllp-server-implementation
# 	# pass
# 	return bootinfo
