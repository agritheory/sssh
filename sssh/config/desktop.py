# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "SSSH",
			"color": "white",
			"icon": "fas fa-hospital",
			"type": "module",
			"label": _("SSSH")
		}
	]
