# #!/usr/bin/python3
import socket
from frappe_client import FrappeRequest
import configparser
import logging
from systemd.daemon import notify
from pathlib import Path


class HL7Service(object):
	socket: ''
	base_url: 'localhost'
	username: "Administrator"
	password: "admin"
	port: 2575
	host: 'localhost'
	server_port: '8003'
	method: 'sssh.api.receive'

	def __init__(self):
		self.load_config()
		self.init_app()
		self.run()

	def load_config(self):
		config = configparser.ConfigParser()
		cwd = Path(__file__).parent.absolute()
		with open(cwd / 'hl7_config.cfg') as cfg:
			config.read_file(cfg)
			self.username = config['Frappe']['username']
			self.password = config['Frappe']['password']
			self.http_port = config['Frappe']['http_port']
			self.port = config['Server']['port']
			self.host = config['Server']['host']
			self.method = config['Server']['method']
			self.base_url = self.validate_url(config['Frappe']['base_url'])

	def init_app(self):
		try:
			s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
			s.bind(("localhost", 2575))
			s.listen(1)
			self.socket = s
			self.client = FrappeRequest(url=self.base_url,
				username=self.username,
				password=self.password)
		except Exception as e:
			print(e)

	def run(self):
		notify('READY=1')
		while True:
			conn, addr = self.socket.accept()
			data = conn.recv(1024)
			conn.close()
			if data:
				s = self.forward(str(data, 'utf-8', 'ignore'))

	def forward(self, data):
		try:
			return self.client.post(self.method, {'data': data})
		except Exception as e:
			print(e)
			# set up log

	def validate_url(self, url):
		if not url.startswith('http'):
			url = "http://" + url
		if self.http_port:
			url = url + ":" + self.http_port
		return url


if __name__ == '__main__':
	h = HL7Service()
