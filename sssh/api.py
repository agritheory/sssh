import frappe
import socket


@frappe.whitelist()
def receive(data):
	data = str(data)
	print(data)


@frappe.whitelist()
def send(data):
	pass


class HL7Sender(object):
	socket: ''
	base_url: 'localhost'
	port: 2575

	def __init__(self):
		self.load_config()

	def load_config(self):
		config = configparser.ConfigParser()
		cwd = Path(__file__).parent.absolute()
		with open(cwd / 'hl7_config.cfg') as cfg:
			config.read_file(cfg)
			self.port = config['Server']['port']
			self.base_url = self.validate_url(config['Frappe']['base_url'])
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect((self.base_url, self.port))
		self.socket = s

	def send(self, data):
		try:
			self.socket.sendall(data.encode('utf-8'))
			data = self.socket.recv(1024)
			self.socket.close()
		except Exception as e:
			print(e)
