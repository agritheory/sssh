## SSSH

ERPNext Customizations for Somali Sudanese Specialized Hospital

#### License

MIT or as otherwise indicated and superseded by Somali Sudanese Specialized Hospital, Mogadishu, Ethiopia.

### User setup
Create a new user that will represent all the incoming data, something like "HL7 Devices".


### Installing the Frappe App
`bench get-app sssh https://gitlab.com/agritheory/sssh.git`

#### Installing the HL7 Server Service

[Reference](https://tecadmin.net/setup-autorun-python-script-using-systemd/)

1) Install required python packages globally:

    sudo apt-get install python-systemd
    sudo pip3 install py-frappe-client hl7

  `hl7` is only required to test the setup

2) Modify `hl7_config.cfg` to have appropriate authorization.

    username=Administrator # change me to a dedicated hl7 api user, not a real person
    password=admin # this is not a secure password


2) From frappe-bench, link `hl7_server.py` to `/usr/bin/`

    sudo ln -s apps/sssh/sssh/hl7_server.py /usr/bin/hl7_server.py
    # verify:
    sudo ls -l apps/sssh/sssh/hl7_server.py /usr/bin/hl7_server.py

3) From frappe-bench, copy `hl7_server.service` to `/lib/systemd/system/` (a symlink won't work here)

    sudo cp apps/sssh/sssh/hl7_server.service /lib/systemd/system/
    # verify:
    sudo ls -l apps/sssh/sssh/hl7_server.service /lib/systemd/system/hl7_server.service

4) Reload, enable and start the service

    sudo systemctl daemon-reload
    sudo systemctl enable hl7_server.service # will run on boot
    sudo systemctl start hl7_server.service # will start immediately

4A) The service can be controlled with the following:

    sudo systemctl stop dummy.service          #To stop running service
    sudo systemctl start dummy.service         #To start running service
    sudo systemctl restart dummy.service       #To restart running service

5) Test the service my pasting the follow command in frappe-bench
    mllp_send --file /apps/sssh/sssh/sample.hl7 --port 2575 --loose

  At this time (Nov 18, 2019) it only prints to the console. Additional work is required in `api.py`

6) nginx modifications:
  WIP
